import XMonad
import XMonad.Layout.Gaps
import qualified Data.Map as M
import XMonad.Util.EZConfig
import XMonad.Layout.Spacing
import XMonad.Layout.ToggleLayouts

myLayout = tiledToggle ||| Mirror tiledToggle ||| Full
  where
	tiled = Tall 1 0.03 0.5
	tiledNoSpacing = tiled
	tiledSpacing = gaps [(U,20),(D,20),(L,20),(R,20)] $ spacing 30 $ tiled
	tiledToggle = toggleLayouts tiledSpacing tiledNoSpacing

myModMask = mod4Mask


--myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
--	[ (())
--	
--	, ((0, 0x1008ff17), spawn "mpc next")
--	, ((0, 0x1008ff16), spawn "mpc prev")
--	, ((0, 0x1008ff14), spawn "mpc toggle") ]

main = xmonad $ defaultConfig
	{ terminal   = "urxvt",
	  layoutHook = myLayout,
--	  keys       = myKeys,i
	  modMask    = myModMask }
	`additionalKeys`
	[ ((0, 0x1008ff17), spawn "mpc next")
	, ((0, 0x1008ff16), spawn "mpc prev")
	, ((0, 0x1008ff14), spawn "mpc toggle")

	, ((mod4Mask, xK_c), spawn "pkill compton || compton -Cc &")
	, ((mod4Mask, xK_s), sendMessage ToggleLayout) ]


